# Example 12-06 fork-server-signal.py - PDF p811

"""
Same as fork-server.py, but use the Python signal module to avoid keeping child
zombie processes after they terminate, instead of an explicit reaper loop before each
new connection; SIG_IGN means ignore, and may not work with SIG_CHLD child exit
signal on all platforms; see Linux documentation for more about the restartability of
socket.accept call interrupted with a signal;
"""

import os, time, sys, signal
from socket import *                        # get socket constructor and constants
myHost = ''                                 # server machine, '' means local host
myPort = 50007                              # listen on a non-reserved port number

sockobj = socket(AF_INET, SOCK_STREAM)          # make a TCP socket object
sockobj.bind((myHost, myPort))                  # bind it to server port number
sockobj.listen(5)                               # allow 5 pending connects
signal.signal(signal.SIGCHLD, signal.SIG_IGN)   # avoid child zombie processes

def now():
    return time.ctime(time.time())              # current time on server

def handleClient(connection):                   # child process: reply, exit
    time.sleep(5)                               # simulate a blocking activity
    while True:                                 # read, write a client socket
        data = connection.recv(1024)            # till eof when socket closed
        if not data: break
        reply = 'Echo=> %s at %s' % (data, now())
        connection.send(reply.encode())
    connection.close()
    os._exit(0)

def dispatcher():                               # listen until process killed
    while True:                                 # wait for next connection
        connection, address = sockobj.accept()  # pass to process for service
        print('Server connected by', address, end=' ')
        print('at', now())
        reapChildren()                          # clean up exited children now
        childPid = os.fork()                    # copy this process
        if childPid == 0:                       # if in childprocess: handle
            handleClient(connection)


dispatcher()