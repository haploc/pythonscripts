"""
Implement an HTTP web server in Python that knows how to run server-side CGI
scrips coded in Python; serves files and scripts from current working dir;
Python scripts must be stored in webdir\cgi-bin or webdir\htbin;
"""

import os, sys
from http.server import HTTPServer, CGIHTTPRequestHandler

webdir = '.' #where your html files and cgi-bin script directories live
port = 9123

os.chdir(webdir)
srvraddr = ('', port)
srvrobj = HTTPServer(srvraddr, CGIHTTPRequestHandler)
srvrobj.serve_forever()

