import os, time

def child(pipeout):
    zzz = 0
    while True:
        time.sleep(zzz)
        msg = ('Spam %03d\n' % zzz). encode()  # Pipes are binary bytes
        os.write(pipeout, msg)
        zzz = (zzz+1) % 5

def parent():
    pipein, pipeout = os.pipe()     # make 2-ended pipe
    if os.fork() == 0:              # in child, write to pipe
        os.close(pipein)            # close input on child side
        child(pipeout)
    else:                           # in parent, read from pipe
        os.close(pipeout)           # close output on parent side
        pipein = os.fdopen(pipein)  # make text mode input file object
        while True:
            line = pipein.readline()[:-1]   # blocks until data sent
            print('Parent %d got [%s] at %s' % (os.getpid(), line, time.time()))

parent()
