""" Collect command-line options in a dictionary  """

def getopts(argv):
    print(argv)
    opts = {}
    while argv:
        print('Processing value: ', argv[0])
        if argv[0][0] == '-':           # find '-name value' pairs
            opts[argv[0]] = argv[1]     # dict key is '-name' arg
            argv = argv[2:]
        else:
            argv = argv[1:]
    return opts

if __name__ == '__main__':
    import sys
    myargs = getopts(sys.argv)
    print(myargs)
    if '-i' in myargs:
        print(myargs['-i'])
    print(myargs)
