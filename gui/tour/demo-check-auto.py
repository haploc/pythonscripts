# Example 8-23 demo-check-manual.py
# Check buttons, the hard way (without variables)

from tkinter import *

root = Tk()
states = []  # change object not name

for i in range(10):
    var = IntVar()
    chk = Checkbutton(root, text=str(i), variable=var)
    chk.pack(side=LEFT)
    states.append(var)

root.mainloop()
print([var.get() for var in states])
print(list(map(IntVar.get, states)))
print(list(map(lambda var: var.get(), states)))