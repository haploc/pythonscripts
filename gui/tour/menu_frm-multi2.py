# Example 9-05 menu_frm-multi2.py

from menu_frm import makemenu
from tkinter import *

def main():
    root = Tk()
    for i in range(3):
        frm = Frame()
        mnu = makemenu(frm)
        mnu.config(bd=2, relief=RAISED)
        frm.pack(expand=YES, fill=BOTH)
        Label(root, bg='white', text='{}'.format(i), height=5, width=25).pack(expand=YES, fill=BOTH)
    Button(root, text="Bye", command=root.quit).pack()
    root.mainloop()

if __name__ == '__main__':
    main()