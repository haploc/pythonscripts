# example 8-28 demo-radio-auto.py
# radio buttons, the easy way

from tkinter import *


def main():
    """
    Insert text here
    """
    root = Tk()  # IntVars work too
    var = IntVar(0)  # select 0 to start
    for i in range(10):
        rad = Radiobutton(root, text=str(i), value=i, variable=var)
        rad.pack(side=LEFT)
    root.mainloop()
    print(var.get())


if __name__ == '__main__':
    main()