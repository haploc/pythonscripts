# Example 8-39

from sys import argv
from tkinter import *


def main():
    filename = argv[1] if len(argv) > 1 else 'ora-lp4e.gif'  # name on cmdline?
    gifdir = "../gifs/"
    win = Tk()
    img = PhotoImage(file=gifdir + filename)
    can = Canvas(win)
    can.pack(fill=BOTH)
    can.config(width=img.width(), height=img.height())
    can.create_image(2, 2, image=img, anchor=NW)
    win.mainloop()

if __name__ == '__main__':
    main()
