# Example 8-37

from tkinter import *


def main():
    gifdir = "../gifs/"
    win = Tk()
    igm = PhotoImage(file=gifdir + "ora-pp.gif")
    Button(win, image=igm).pack()
    win.mainloop()

if __name__ == '__main__':
    main()