"""
# Version 1 :
def scanner(name, function):
    file = open(name, 'r')
    while True:
        line = file.readline()
        if not line: break
        function(line)
    file.close()
"""
"""
# Alternatives
# loop with iterators
def scanner(name, function):
    for line in open(name, 'r'):
        function(line)

# with map
def scanner(name, function):
    list(map(function, open(name, 'r')))

# with generators
def scanner(name, function):
    list(function(line) for line in open(name, 'r'))
""" 
# with list comprehension
def scanner(name, function):
    [ function(line) for line in open(name, 'r') ]
